ymaps.ready(init);
function init () {
    var myMap = new ymaps.Map("map", {
    //определяем местоположение пользователя 
    center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude], zoom: 12})
    //определяем город пользователя
    var town=ymaps.geolocation.city;
    var data_send=new Object();
    data_send.town = town;
    var coords=[];
    var address=[];
    var names_shop=[];
    var json_array;
    jQuery.ajax(
    {
     	url:"json_response.php",
     	data: data_send,
        dataType: 'json',
     	success: function(data){
     		json_array=data;
     		for(key in json_array){
     		coords.push(json_array[key].coordinates);
     		address.push(json_array[key].address);
     		names_shop.push(json_array[key].name_shop);
     		}
        //Ставим метки	  	
        var myCollection = new ymaps.GeoObjectCollection({}, {preset: 'twirl#shopIcon'});
	        for (var i = 0; i < coords.length; i++){
    		myCollection.add(new ymaps.Placemark(coords[i],
            {
                   balloonContentHeader:names_shop[i],
                   balloonContent: address[i],
             }));
	 }
    myMap.geoObjects.add(myCollection);
     }
   });
//настройки карты
myMap.controls.add(new ymaps.control.ZoomControl());
myMap.controls.add('mapTools');
myMap.controls.add('typeSelector');
}